package com.example.appbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;


public class NXTController {
    private String address = "";
    private BluetoothDevice mBluetoothLEGO;
    private BluetoothSocket mSocketLEGO;
    private String fwVersion = "unknown";
    private Context ctx;
    public final static String ACTION_CONNECTION_CHANGED="com.example.appbluetooth.ACTION_CONNECTION_CHANGED";
    public NXTController(Context ctx, String address) {
        this.address = address;
        this.ctx = ctx;
        mBluetoothLEGO = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);

    }

    public boolean isConnected() {
        boolean ret = false;
        if (mSocketLEGO != null) {
           // ret = mSocketLEGO.isConnected();// Not available in API <14
            ret= true;
        }
        return ret;
    }

    public boolean connect() {

        Toast.makeText(ctx, "Connecting", Toast.LENGTH_SHORT).show();
        new CreateSocketAsyncTask().execute();

        return false;
    }

    public void getFWVersion() {

        if (!isConnected()) {
            LoggerConsole.Log("Error : NXT not connected");
        }
        Toast.makeText(ctx, "Asking NXT for FW version", Toast.LENGTH_SHORT).show();

        new GetFWVersionAsyncTask().execute();

    }

    public void beep() {
        Toast.makeText(ctx, "Asking for beep", Toast.LENGTH_SHORT).show();
        new SendBeepAsyncTask().execute();
    }

    public String printFwVersion() {
        return fwVersion;
    }


    /////////////////////////////////////////////////////////////////
    // Asynctasks to execute blocking methods in a separate thread
    ///////////////////////////////////////////////////////////////

    class CreateSocketAsyncTask extends AsyncTask<String, Integer, Integer> {
        @Override
        protected Integer doInBackground(String... params) {

            try {

                if (mSocketLEGO == null) {
                    mSocketLEGO = mBluetoothLEGO.createRfcommSocketToServiceRecord(
                            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    Log.d(LoggerConsole.TAG, "Connecting to lego NXT");
                    // Connect the device through the socket.
                    // This will block until it succeeds or throws an exception
                    mSocketLEGO.connect();

                }

                // check for the availability of streams.
                mSocketLEGO.getInputStream();
                mSocketLEGO.getOutputStream();

            } catch (java.io.IOException e) {
                // something wrong -> not connected
                mSocketLEGO = null;
                Log.d(LoggerConsole.TAG, "Unexpected Exception" + e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if (mSocketLEGO!=null) {
                LoggerConsole.Log("Connected succesfully");
            } else {
                LoggerConsole.Log("Error connecting");
            }

           LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(ACTION_CONNECTION_CHANGED));
        }
    }



    class GetFWVersionAsyncTask extends AsyncTask<String, Integer, Integer> {
        @Override
        protected Integer doInBackground(String... params) {
            NXTController.this.fwVersion = NXTController.this.sendFWVersionCMD();
            return null;
        }
    }

    class SendBeepAsyncTask extends AsyncTask<String, Integer, Integer> {
        @Override
        protected Integer doInBackground(String... params) {
            sendBeepCMD();
            return null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    // Functions for managing the data exchange with LEGO NXT platform
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Send a command for retreiving de FW version and waits for the rresponse.
     * @return String representing de version as "Major.Minor"
     */

    private String sendFWVersionCMD() {
        String version = "unk";
        byte[] response = new byte[128];


        if(isConnected()==false) {
            Log.e(LoggerConsole.TAG, "Not sendding beep, reason :Not connected");
        }else {

            try {
                // discard prevous data in input buffer
                mSocketLEGO.getInputStream().skip(mSocketLEGO.getInputStream().available());
                // get firmware version
                byte[] command = {0x01, (byte) 0x88};
                // adapting the command
                byte[] command_extended = addLengthAndResponse(command);
                // send command
                mSocketLEGO.getOutputStream().write(command_extended);

                // wait for response
                int len1 = mSocketLEGO.getInputStream().available();
                while (len1 == 0) {
                    // No data received yet!!!
                    SystemClock.sleep(100);
                    len1 = mSocketLEGO.getInputStream().available();
                }

                int len2 = mSocketLEGO.getInputStream().read(response);
                if (len1 != len2) {
                    Log.e(LoggerConsole.TAG, "sendFWVersionCMD() : amount of read data deferent form expected");
                } else {
                    byte[] data = getBody(response); // adapting the Lego response
                    if (data[0] == (byte) 0x02 && data[1] == (byte) 0x88) { // checking for a valid response
                        version = String.valueOf(data[6]) + "." + String.valueOf(data[5]);
                    }
                }
            } catch (IOException e) {
                // something wrong -> assume conection had been lost
                Log.e(LoggerConsole.TAG, "Unexepected exception" + e);
                mSocketLEGO = null;

                // anounce new conection state
                LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(ACTION_CONNECTION_CHANGED));

            }
        }

        return version;
    }

    /**
     * Send a beep command to the NXT if conected
     */
    private void sendBeepCMD() {
        byte[] response = new byte[128];
        byte[] command = {0x00, (byte) 0x03, (byte) 0xE8, (byte) 0x03, (byte) 0xE8, (byte) 0x03};

        if(isConnected()==false) {
            Log.e(LoggerConsole.TAG, "Not sendding beep, reason :Not connected");
        }else {
            Log.e(LoggerConsole.TAG, "Sending beep");
            try {
                byte[] command_extended = addLengthAndResponse(command);
                // send command
                mSocketLEGO.getOutputStream().write(command_extended);


                int len1 = mSocketLEGO.getInputStream().available();
                while (len1 == 0) {
                    // No data received yet!!!
                    SystemClock.sleep(100);
                    len1 = mSocketLEGO.getInputStream().available();
                }

                int len2 = mSocketLEGO.getInputStream().read(response);

            } catch (IOException e) {
                // something wrong
                mSocketLEGO = null;
                Log.e(LoggerConsole.TAG, "Unexepected exception" + e);
                // announce new connection state
                LocalBroadcastManager.getInstance(ctx).sendBroadcast(new Intent(ACTION_CONNECTION_CHANGED));
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////
    //   NXT UTILS
    /////////////////////////////////////////////////////////////////////////


    // preffix the bytes with their length
    private static byte[] addLengthAndResponse(byte[] c)
            throws IllegalArgumentException {
        int l = c.length;
        if (l <= 0) throw new IllegalArgumentException("Null message length");
        // max. length of a message must be 64+2 bytes, according to the NXT
        byte[] len = getLMSBfromInt(l);
        byte[] result = new byte[l + 2];
        System.arraycopy(len, 0, result, 0, 2);
        System.arraycopy(c, 0, result, 2, l);
        return (result);
    }

    private static byte[] getBody(byte[] msg)
            throws IllegalArgumentException {
        if (msg.length < 3)
            throw new IllegalArgumentException("Message with <3 bytes");
        int leninmsg = getIntFromLMSB(msg[0], msg[1]);
        byte[] bdata = new byte[leninmsg];
        System.arraycopy(msg, 2, bdata, 0, leninmsg);
        return (bdata);
    }

    // return LSB+MSB bytes of an int
    private static byte[] getLMSBfromInt(int v) throws IllegalArgumentException {
        if ((v > 65535) || (v <= 0))
            throw new IllegalArgumentException("Invalid word");
        byte[] result = new byte[2];
        result[0] = (byte) (v & 0xff);
        result[1] = (byte) ((v & 0xff00) >> 8);
        return (result);
    }


    /**
     * return the int corresponding to LSB+MSB bytes
     *
     * @param lsb
     * @param msb
     * @return
     */
   private static int getIntFromLMSB(byte lsb, byte msb) {
        int l = (lsb < 0 ? 256 + (int) lsb : lsb);
        int m = (msb < 0 ? 256 + (int) msb : msb);
        return (l + (m << 8));
    }
}
