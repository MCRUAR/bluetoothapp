package com.example.appbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Set;


public class MainActivity extends AppCompatActivity {
    private final static int REQUEST_ENABLE_BT = 1; // it must be greater than 0
    private static final String MAC_LEGO = "C0:F8:DA:E5:10:FA";
    BluetoothAdapter mBluetoothAdapter;
    BroadcastReceiver mReceiver;
    NXTController nxtController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        mReceiver = new BluetoothBroadcastReceiver();
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy

        registerReceiver(mReceiver,new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,new IntentFilter(NXTController.ACTION_CONNECTION_CHANGED));
        nxtController = new NXTController(this, MAC_LEGO);
        LoggerConsole.init(findViewById(R.id.activity_main));


        actualizarBotones();
    }

    ///////////////////////////////////////BLUETOOTH/////////////////////////////////////////////
    public void ConectarBluetooth(View view) {
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    //Respuesta a la activación del bluetooth
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        actualizarBotones();
    }

    public void MostrarEmparejados(View view) {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.select_dialog_singlechoice);
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                LoggerConsole.Log("Paired Device: name=" + device.getName() + ",MAC=" + device.getAddress());
                arrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } else {
            LoggerConsole.Log("There is no paired devices");
        }

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
        builderSingle.setIcon(android.R.drawable.stat_sys_data_bluetooth);
        builderSingle.setTitle("Paired devices:");


        builderSingle.setNegativeButton(
                android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                null);

        SystemClock.sleep(200);
        //builderSingle.show();
    }

    public void BuscarDispositivos(View view) {
        mBluetoothAdapter.startDiscovery();
    }

    public void CancelarBusqueda(View view) {
        mBluetoothAdapter.cancelDiscovery();
    }

    ///////////////////////////////////////LEGO//////////////////////////////////////////////
    public void conectalego(View view) {
        nxtController.connect();
       // actualizarBotones();
    }

    public void solicitaVersion(View view) {
        nxtController.getFWVersion();
    }

    public void imprimirVersion(View view) {
        LoggerConsole.Log("Versión:"+ nxtController.printFwVersion()+"\n");
        Toast.makeText(this,"Versión:"+ nxtController.printFwVersion()+"\n",Toast.LENGTH_SHORT).show();
    }

    public void SolicitarBeep(View view) {
        nxtController.beep();
    }

    public void actualizarBotones(){
        findViewById(R.id.buscar_dispositivos).setEnabled(mBluetoothAdapter.isEnabled());
        findViewById(R.id.mostrar_emparejados).setEnabled(mBluetoothAdapter.isEnabled());
        findViewById(R.id.cancelar_busqueda).setEnabled(mBluetoothAdapter.isEnabled());
        ((CheckBox)findViewById(R.id.check_bluetooth)).setChecked(mBluetoothAdapter.isEnabled());
        findViewById(R.id.conecta_lego).setEnabled(mBluetoothAdapter.isEnabled());
        findViewById(R.id.solicitar_version).setEnabled(mBluetoothAdapter.isEnabled() && nxtController.isConnected());
        findViewById(R.id.imprimir_version).setEnabled(mBluetoothAdapter.isEnabled() && nxtController.isConnected());
        findViewById(R.id.solicitar_beep).setEnabled(mBluetoothAdapter.isEnabled() && nxtController.isConnected());
        ((CheckBox)findViewById(R.id.checkLego)).setChecked(mBluetoothAdapter.isEnabled() && nxtController.isConnected());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }


    // Class for a BroadcastReceiver for managing Bluetooth ACTION_* events
    class BluetoothBroadcastReceiver extends BroadcastReceiver {
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            String action = intent.getAction();
            // A bluetooth device has been found
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a Toast
                LoggerConsole.Log("New device found : "+device.getName() + " - " + device.getAddress() + '\n');

            }else  {
                actualizarBotones();


            }
    }
    }



}
