package com.example.appbluetooth;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Created by abdel on 24/11/16.
 */

public class LoggerConsole {

    public final static String TAG="bluetoothapp";
    private static TextView consoleTxtView;
    private static ScrollView consoleScrollView;

    static void init(View v)
    {
        consoleTxtView= (TextView) v.findViewById(R.id.consola);
        consoleScrollView= (ScrollView) v.findViewById(R.id.consola_container);

          }

    /**
     * Print a message to Console view and scrolls.
     * Only run in main thread
     * @param s
     */
    static public void Log(String s)
    {

        if(consoleScrollView != null && consoleTxtView!=null)
        {
            consoleTxtView.setText(s+"\n"+consoleTxtView.getText());
            consoleScrollView.fullScroll(View.FOCUS_UP);
        }
        Log.d(TAG,s);
    }
}
