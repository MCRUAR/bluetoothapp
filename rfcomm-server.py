#!/usr/bin/env python
from bluetooth import *
import binascii
server_sock=BluetoothSocket( RFCOMM )
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "00001101-0000-1000-8000-00805F9B34FB"

advertise_service( server_sock, "SampleServer",
                   service_id = uuid,
                   service_classes = [ uuid, SERIAL_PORT_CLASS ],
                   profiles = [ SERIAL_PORT_PROFILE ],
#                   protocols = [ OBEX_UUID ]
                    )

print "Waiting for connection on RFCOMM channel %d" % port

while True:

    client_sock, client_info = server_sock.accept()
    client_sock.send("HOLA\r\n")
    print "Accepted connection from ", client_info

    try:
        while True:
            data = client_sock.recv(1024)
            if len(data) == 0: break
            print "received [%s]" % binascii.hexlify(data)
            client_sock.send( binascii.unhexlify("080002880102031F0106")+"\r\n");

    except IOError:
        pass

    print "disconnected"

    client_sock.close()
server_sock.close()
print "all done"